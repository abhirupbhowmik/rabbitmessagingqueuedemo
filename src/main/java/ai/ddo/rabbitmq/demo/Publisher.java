package ai.ddo.rabbitmq.demo;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

@Component
public class Publisher implements CommandLineRunner {

    private final RabbitTemplate rabbitTemplate;
    private final Receiver receiver;

    public Publisher(Receiver receiver, RabbitTemplate rabbitTemplate) {
        this.receiver = receiver;
        this.rabbitTemplate = rabbitTemplate;
    }

    @Override
    public void run(String... args) throws Exception {
        System.out.println("Sending message to TOPIC_EXCHANGE : " + Application.topicExchangeName);
        rabbitTemplate.convertAndSend(Application.topicExchangeName, "ddo.abhirup.message", "Hello to ABHIRUP !!");
        receiver.getLatch().await(10000, TimeUnit.MILLISECONDS);
    }

}
